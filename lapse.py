#!/usr/bin/env python2

"""Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-03-29 20:40:48 (jmiller)>

This is part of the adm_spherical_symmetry module which solves the ADM equations
for gravity in spherical symmety. This module solves the lapse.
"""

# Imports
# ----------------------------------------------------------------------
import numpy as np
from scipy import optimize

import data_structures
import boundary_data
import rhs
from orthopoly import PseudoSpectralStencil
from data_structures import A_I,K_I,RHO_I,J_I,S_I
# ----------------------------------------------------------------------


# ======================================================================
# Lapse Residual
# ======================================================================
def get_lapse_residual(r,a,krr,rho,jr,s,alpha,alpha_prime,alpha_prime_prime):
    """
    Calculates the residual of hte lapse. If this vanishes, over all
    space,then the the slicing condition for the lapse is satisfied.

    WARNING: This does not include boundary data

    Parameters
    ----------
    -- r, the radius
    -- a, the square root of the (r,r)-component of the metric.
    -- krr, extrinsic curvature (with one index up)
    -- rho, the matter energy density
    -- jr, the radial component of the matter current (index up)
    -- s, the trace of the stress tensor.
    -- alpha, the lapse
    -- alpha_prime, the derivative of the lapse
    -- alpha_prime_prime, the second derivative of the pase
    """
    a_prime = rhs.get_a_prime(r,a,krr,rho,jr,s)
    residual = (a*r*alpha_prime_prime - a_prime*r*alpha_prime + 2.*a*alpha_prime
                - (a**3) * alpha * r* ((3./2)* (krr**2) + 4*np.pi*(rho + s)))
    return residual


class LapseResidual:
    """
    A container class. Can be called to return the residual of the lapse
    as defined on a stencil grid.
    """
    def __init__(self,stencil,hypersurface_variables,matter_variables):
        """
        Initializes the LapseResidual object.

        Parameters
        ---------
        stencil                -- An instance of the PseudoSpectralStencil object,
                                  defining the grid.
        hypersurface_variables -- Array-like. Each row is of the form [a,krr],
                                  and gives these variables at the colocation point
                                  with the same index as the row.
        matter_variables       -- Vector-valued function. Returns [rho,jr,s] for
                                  a given value of r.
        """
        # basics
        self.stencil = stencil
        self.r = stencil.get_x()
        # matter
        matter_vector = np.array([matter_variables(x) for x in self.r])
        self.rho = matter_vector[...,RHO_I]
        self.jr = matter_vector[...,J_I]
        self.s = matter_vector[...,S_I]
        # hypersurface
        self.a = hypersurface_variables[...,A_I]
        self.krr = hypersurface_variables[...,K_I]
        self.a_prime = rhs.get_a_prime(self.r,self.a,self.krr,self.rho,self.jr,self.s)

    def residual(self,alpha):
        """
        Given a value for the lapse alpha, calculates its residual
        """
        alpha_prime = self.stencil.differentiate(alpha)
        alpha_prime_prime = self.stencil.differentiate(alpha_prime)
        # Get the residual in the bulk
        residual= get_lapse_residual(self.r,self.a,self.krr,
                                     self.rho,self.jr,self.s,
                                     alpha,alpha_prime,alpha_prime_prime)
        # Set the boundary data for the residual
        residual[0] = boundary_data.origin_lapse_residual(alpha[0],alpha_prime[0])
        residual[-1] = boundary_data.asymptotic_lapse_residual(self.r[-1],
                                                               alpha[-1],
                                                               alpha_prime[-1])
        return residual
# ======================================================================



# ======================================================================
# Lapse initial guess
# ======================================================================
def get_lapse_initial_guess(stencil):
    """"
    Given a stencil for the lapse, return a guess for what it should be everywhere
    """
    return 10*np.ones_like(stencil.get_x())
# ======================================================================



# ======================================================================
# Solve for the lapse
# ======================================================================
def solve_for_lapse(stencil,hypersurface_variables,
                    matter_variables):
    """
    Solves for the lapse given the solution on the hypersurface.

    Parameters
    ---------
    stencil                -- An instance of the PseudoSpectralStencil object,
                              defining the grid.
    hypersurface_variables -- Array-like. Each row is of the form [a,krr],
                              and gives these variables at the colocation point
                              with the same index as the row.
    matter_variables       -- Vector-valued function. Returns [rho,jr,s] for
                              a given value of r.
    """
    alpha0 = get_lapse_initial_guess(stencil)
    residual = LapseResidual(stencil,hypersurface_variables,matter_variables)
    soln = optimize.newton_krylov(residual.residual,alpha0,f_tol=1E-10)
    return soln
#     soln = optimize.fsolve(residual.residual,alpha0)
#     if not soln.success:
#         print soln.message
#         assert soln.success
#     else:
#         print soln.x
#         return soln.x

# ======================================================================



# Warning not to run this program on the command line
if __name__ == "__main__":
    raise ImportError("Warning. This is a library. It contains no main function.")
