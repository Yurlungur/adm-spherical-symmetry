#!/usr/bin/env python2

"""Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-03-29 22:46:11 (jmiller)>

This module is part of the adm_spherical_symmetry package, which
solves the ADM equations for gravity in spherical symmetry.

This module defines the matter content of the hypersurface. It defines
all quantities in the weak-field limit where the energy-momentum
tensor is defined around Minkowski space.

We define a number of matter_functions, meant to be fed into
rhs.get_hypersurface_prime.  Each of these functions is an
array-valued function of the radius r. It is a map:
r -> np.array([rho,jr,S])
where:
-- rho, the matter energy density
-- jr, the radial component of the matter current (index up)
-- S, the trace of the stress tensor.

We also define a number of helper functions.

We use natural units where c = 1.
"""

# Imports
# ----------------------------------------------------------------------
import numpy as np
import data_structures
from data_structures import A_I,K_I,RHO_I,J_I,S_I
# ----------------------------------------------------------------------

def get_gaussian(r,amplitude,center,width):
    """
    A Gaussian function with the given amplitude centered at r=center
    with standard deviation width
    """
    return amplitude*np.exp(-(r-center)**2/(2*width**2))

def get_lorentz_factor(v):
    """
    Given a radial velocity v (all other velocities vanish),
    gives the Lorentz factor gamma.
    """
    return 1.0/np.sqrt(1 - v**2)

def get_four_velocity(v):
    """
    Given a radial velocity v (all other velocities vanish),
    gives the four-velocity u^mu
    """
    return np.array([get_lorentz_factor(v),
                     v, 0, 0])

def uniformly_infalling_gaussian_dust(r,amplitude,center,width,vr):
    """
    Returns the matter_vector for a pressure-less gas with
    a Gaussian profile with the given amplitude, center, and width moving
    uniformly with radial velocity vr
    """
    out = np.empty(data_structures.NUM_MATTER_VARIABLES)
    out[RHO_I] = get_gaussian(r,amplitude,center,width)
    out[J_I] = vr*out[RHO_I]
    out[S_I] = 0
    return out
    
def vacuum(r):
    """
    Returns the matter_vector for a pure vacuum hypersurface as a
    function of the radius r.
    """
    return np.zeros(data_structures.NUM_MATTER_VARIABLES)

def gaussian_star(r,amplitude=1./50,offset=0.,width=1.,stress=0,n=1):
    """
    A generic Gaussian distribution of matter with no infall. By default gives values
    that Alex Terrana uses. For double-checking.

    If stress !=0, has a polytropic equation of state with polytropic index n
    """
    rho = amplitude*np.exp(-(r-offset)**2/(2.*width*width))
    return np.array([rho,0,3*stress*rho**((n+1)/float(n))])


    
